# Pokemon APP
---
# Description

This app just has a custom django command to get the evolution chains information from a chain id, and a simple  API to serve the pokemon information using as parameter just a pokemon name.

# Installation

* Install requirements.
    ```sh
    pip install -r requirements.txt
    ```
* Install Database: The DB engine is MySQL. Create the database 'pokemon_app', then in the directory 'pokemon_app/pokemon_app/' you must create the file 'settings_local.py' with your databases django connections:

    ```python
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'pokemon_app',
            'USER': 'root',
            'PASSWORD': 'mypassword',
            'HOST': 'localhost',
            'PORT': '3306',
        },
    }
    ```
* Run tge migrations:
    ```sh
    python manage.py migrate
    ```
* Create super user:
    ```sh
    python manage.py createsuperuser
    ```
* Run the project:
    ```sh
    python manage.py runserver localhost:8000
    ```

# Commands

* get_evolution_chain: Gets an evolution chain from pokeapi using a chain id.

    Example:
    
    ```sh
    python manage.py get_evolution_chain 10
    ```
# Views

* [http://127.0.0.1:8000/](http://127.0.0.1:8000/): Project home
* [http://127.0.0.1:8000/admin/](http://127.0.0.1:8000/admin/): Django admin.

# Api

Serves the pokemon information using as parameter just a pokemon name.

Endpoint:

[http://127.0.0.1:8000/api/v1/pokemon/{name}](http://127.0.0.1:8000/api/v1/pokemon/{name})


## Example:

[http://127.0.0.1:8000/api/v1/pokemon/pichu](http://127.0.0.1:8000/api/v1/pokemon/pichu)