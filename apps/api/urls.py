from django.urls import path
from apps.api.views import PokemonRetrieveAPIView

urlpatterns = [
    path('pokemon/<str:name>', PokemonRetrieveAPIView.as_view(), name='pokemon_retrieve_api_view'),
    ]