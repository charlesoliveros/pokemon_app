from apps.pokemon.models import Pokemon, BaseStat, Evolution
from rest_framework import serializers


class EvolutionSerializer(serializers.ModelSerializer):

    evolves_to = serializers.SerializerMethodField()

    def get_evolves_to(self, instance):
        evolutions = Evolution.objects.filter(pokemon__name=instance.name)
        if evolutions.exists():
            serializer = EvolutionSerializer(evolutions, many=True)
            return serializer.data

    class Meta:
        model = Evolution
        fields = ['evolution_type', 'pokemon_api_id', 'name', 'evolves_to',]


class BaseStatSerializer(serializers.ModelSerializer):

    class Meta:
        model = BaseStat
        fields = ['category', 'base_stat',]


class PokemonSerializer(serializers.ModelSerializer):

    stats = BaseStatSerializer(source='basestat_set', many=True)
    evolves_to = EvolutionSerializer(source='evolution_set', many=True)

    class Meta:
        model = Pokemon
        fields = ['pokemon_api_id', 'name', 'height', 'weight', 'stats', 'evolves_to']