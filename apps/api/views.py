from rest_framework.generics import RetrieveAPIView
from apps.pokemon.models import Pokemon
from apps.api.serializers import PokemonSerializer


class PokemonRetrieveAPIView(RetrieveAPIView):

    lookup_field = 'name'
    queryset = Pokemon.objects.all()
    serializer_class = PokemonSerializer

