from django.db import models


class Pokemon(models.Model):
    ''' Saves the pokemon basic information '''

    pokemon_api_id = models.IntegerField(unique=True, null=False, blank=False)
    name = models.CharField(max_length=200, unique=True, null=False, blank=False)
    height = models.IntegerField(null=True, blank=True)
    weight = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


class BaseStat(models.Model):
    ''' Saves the Pokemon Base Stat of the six Categories'''

    CATEGORIES = (
        ('hp', 'HP'),
        ('attack', 'Attack'),
        ('defense', 'Defense'),
        ('special-attack', 'Special Attack'),
        ('special-defense', 'Special Defense'),
        ('speed', 'Speed'),
        )

    pokemon = models.ForeignKey(Pokemon, on_delete=models.DO_NOTHING)
    category = models.CharField(choices=CATEGORIES, max_length=100, null=True, blank=True)
    base_stat = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return '{}: {}'.format(self.pokemon.name, self.base_stat)


class Evolution(models.Model):
    ''' Saves the evolutions of a pokemon'''

    EVOLUTION_TYPES = (
        ('preevolution', 'Preevolution'),
        ('evolution', 'Evolution'),
        )

    pokemon = models.ForeignKey(Pokemon, on_delete=models.DO_NOTHING)
    evolution_type = models.CharField(choices=EVOLUTION_TYPES, max_length=100, null=True, blank=True)
    pokemon_api_id = models.IntegerField(unique=True, null=False, blank=False)
    name = models.CharField(max_length=200, unique=True, null=False, blank=False)

    def __str__(self):
        return '{} -> {}'.format(self.pokemon.name, self.name)
