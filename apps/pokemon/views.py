from django.shortcuts import render
from rest_framework.reverse import reverse


def index(request):
    context = {'api_url': reverse('pokemon_retrieve_api_view', args=['pichu'], request=request)}
    return render(request, 'pokemon/index.html', context)