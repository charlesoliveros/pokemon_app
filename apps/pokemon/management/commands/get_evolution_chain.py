from django.core.management.base import BaseCommand
from apps.pokemon.pokeapi import get_and_save_evolution_chain


class Command(BaseCommand):
    
    help = 'Gets a evolution chain from pokeapi using a chain id'

    def add_arguments(self, parser):
        parser.add_argument('chain_id', type=int, help='Chain ID from pokeapi')

    def handle(self, *args, **kwargs):
        get_and_save_evolution_chain(kwargs['chain_id'])
