from django.contrib import admin
from apps.pokemon.models import Pokemon, BaseStat, Evolution


class PokemonAdmin(admin.ModelAdmin):
    list_display = ('name', 'pokemon_api_id',)
    search_fields = ('name',)


class BaseStatAdmin(admin.ModelAdmin):
    list_display = ('pokemon', 'category', 'base_stat',)
    search_fields = ('pokemon__name', 'category',)

    def pokemon(self, instance):
        return instance.pokemon.name


class EvolutionAdmin(admin.ModelAdmin):
    list_display = ('pokemon', 'evolution_type', 'name', 'pokemon_api_id',)
    search_fields = ('pokemon__name',)

    def pokemon(self, instance):
        return instance.pokemon.name


admin.site.register(Pokemon, PokemonAdmin)
admin.site.register(BaseStat, BaseStatAdmin)
admin.site.register(Evolution, EvolutionAdmin)