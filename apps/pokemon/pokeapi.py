import requests
import json
from django.conf import settings
from apps.pokemon.models import Pokemon, BaseStat, Evolution


def get_pokeapi_data(endpoint_complement):
    ''' Receives a pokeapi endpoint and execute a get request
        Returns a dict with the text response if it exists
    '''
    url = '{}/{}'.format(settings.POKEAPI_URL, endpoint_complement)
    print('GET {}'.format(url))
    try:
        response = requests.get(url)
    except Exception as err:
        print('Error: {}'.format(str(err)))
    else:
        print('RESPONSE CODE: {}'.format(response.status_code))
        if response.status_code == 200:
            if response.text:
                chain = json.loads(response.text)
                return chain
            else:
                print('RESPONSE TEXT IS EMPTY')


def get_evolution_chain(chain_id):
    ''' Receives a chain id and gets all the information chain from the pokeapi using the chain id
    '''
    METHOD = 'evolution-chain'
    endpoint_complement = '{}/{}'.format(METHOD, chain_id)
    return get_pokeapi_data(endpoint_complement)


def save_evolution_chain(chain={}, pokemon_data={}):
    ''' Receives a chain as a dict and save them in database 
    '''

    if chain and chain.get('species'):
        pokemon_name = chain['species']['name']
        if not pokemon_data:
            pokemon_data = get_pokemon(pokemon_name)

        if pokemon_data:
            pokemon, pokemon_created = Pokemon.objects.get_or_create(
                pokemon_api_id=pokemon_data['id'], name=pokemon_name
                )
            pokemon.weight = pokemon_data.get('weight')
            pokemon.height = pokemon_data.get('height')
            pokemon.save()

            ''' Stats information'''
            stats = pokemon_data.get('stats')
            for stat_data in stats:
                stat_name = stat_data.get('stat').get('name') if stat_data.get('stat') else None
                if stat_name:
                    stat, stat_created = BaseStat.objects.get_or_create(
                        pokemon=pokemon, category=stat_name
                        )
                    stat.base_stat = stat_data.get('base_stat')
                    stat.save()
            
            ''' Evolutions information'''
            evolutions = chain.get('evolves_to')
            if evolutions:
                for evolution_data in evolutions:
                    if evolution_data.get('species'):
                        evolution_name = evolution_data.get('species')['name']
                        envolve_pokemon_data = get_pokemon(evolution_name)
                        if envolve_pokemon_data:
                            evolution = None
                            try:
                                evolution = Evolution.objects.get(pokemon=pokemon)
                            except Evolution.DoesNotExist:
                                evolution = Evolution(pokemon=pokemon)

                            evolution.evolution_type = 'evolution'
                            evolution.pokemon_api_id = envolve_pokemon_data['id']
                            evolution.name = evolution_name
                            evolution.save()
                            save_evolution_chain(chain=evolution_data, pokemon_data=envolve_pokemon_data)

            
def get_and_save_evolution_chain(chain_id):
    ''' Receives a chain id, gets all the information chain 
        from the pokeapi using the chain id and save them in database 
    '''
    chain_data = get_evolution_chain(chain_id)

    if chain_data and chain_data.get('chain'):
        try:
            save_evolution_chain(chain_data.get('chain'))
        except KeyError as err:
            print('KeyError: {}'.format(err))


def get_pokemon(name):
    ''' Receives a pokemon name and gets all its data from pokeapi
    '''
    METHOD = 'pokemon'
    endpoint_complement = '{}/{}'.format(METHOD, name)
    return get_pokeapi_data(endpoint_complement)
